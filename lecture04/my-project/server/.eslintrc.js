module.exports = {
  env: {
    node: true,
    es6: true
  },
  plugins: ['node'],
  extends: ['eslint:recommended', 'plugin:node/recommended'],
  rules: {
    'node/exports-style': ['error', 'module.exports']
  }
}
