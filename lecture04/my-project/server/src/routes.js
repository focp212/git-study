module.exports = app => {
  app.get('/', (req, res) => {
    try {
      res.send('Hello World');
    } catch (err) {
      res.status(400).send({
        error: 'Error!'
      });
    }
  });
};
